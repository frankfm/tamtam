require('../sass/main.scss');
import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Contact from "./pages/Contact";
import Home from "./pages/Home";
import Layout from "./pages/Layout";
import People from "./pages/People";

const app = document.getElementById('app');

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={Home} />
            <Route path="people" component={People} />
            <Route path="contact" component={Contact} />
        </Route>
    </Router>,
app);


// var url = 'https://instagram.com/tamtamnl/media';
// var request = new XMLHttpRequest();
// request.open('GET', url);
// request.onload = function() {
//     if (request.status == 200) {
//         showData(request.responseText);
//     }
// };
// request.send(null);
//
// function showData(responseText) {
//     var container = document.getElementsByClassName('instagram-feed')[0];
//     var data = JSON.parse(responseText);
//     let items = data.items;
//     let images = [];
//     let captions = [];
//     items.forEach(function(item){
//         images.push(item.images.low_resolution.url);
//     })
//     items.forEach(function(item){
//         captions.push(item.caption.text);
//     })
// }

