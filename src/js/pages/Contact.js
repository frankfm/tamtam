import React from "react";
import { Link } from "react-router";

import Title from "../components/contact/Title";
import ErrorMessage from "../components/contact/ErrorMessage";
import Form from "../components/contact/Form";

export default class Contact extends React.Component {

    render() {
        return (
            <div class="contact">
                <Title />
                <ErrorMessage />
                <Form />
            </div>
        );
    }
}



