import React from "react";
import { Link } from "react-router";

import Slider from "../components/home/Slider";
import Intro from "../components/home/Intro";
import Feed from "../components/home/Feed";

export default class Home extends React.Component {
    render() {
        const slides = [
            <div class="slide" id="slide1"></div>,
            <div class="slide" id="slide2"></div>,
            <div class="slide" id="slide3"></div>
        ];
        const titles = ['Walibi', 'Florensis', 'Oxxio'];
        return (
        <main class="main">
            <Slider slides={slides} titles={titles}/>
            <Intro/>
            <Feed />
        </main>
        );
    }
}

