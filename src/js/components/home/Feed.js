import React from "react";

export default class Feed extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <section class="follow-us">
                <h1 class="title">Follow us on instagram</h1>
                <h2 class="subtitle">@ tamtamnl</h2>
                <div class="feeds">
                    <div class="feed">
                        <div class="feed-image" id="feed-image-1" alt="instagram_feed"></div>
                        <p class="feed-text">Last Thursday emerce organised their own ADE and we were part of it! Where are you gonna party tonight? #ADE#tbt #enight #tamtam</p>
                    </div>
                    <div class="feed">
                        <div class="feed-image" id="feed-image-2" alt="instagram_feed"></div>
                        <p class="feed-text">We're feeling happier and happier thanks to these nice dutch flowers we received from Florensis! #holland #flowers #doordebloemenofficenietmeerzien #settingthebloemetjesoutside</p>
                    </div>
                    <div class="feed">
                        <div class="feed-image" id="feed-image-3" alt="instagram_feed"></div>
                        <p class="feed-text">We are VERY VERY happy with our Silver Lovie Award and People's Lovies Winner for the Docters without borders website! #lovies #tamtamhome #proud #celebration</p>
                    </div>
                    <div class="feed">
                        <div class="feed-image" id="feed-image-4" alt="instagram_feed"></div>
                        <p class="feed-text">Thanks @radio538 and @maxpinas for bringing us friday beats with our brand new #marshall speakers! #friyay #gift #speaker #tamtamhome</p>
                    </div>
                    <div class="feed">
                        <div class="feed-image" id="feed-image-5" alt="instagram_feed"></div>
                        <p class="feed-text">Friyay! Coffee anyone? #almostweekend #tamtamhome #koffiemax #latteart</p>
                    </div>
                    <div class="feed">
                        <div class="feed-image" id="feed-image-6" alt="instagram_feed"></div>
                        <p class="feed-text">TamTam Talks Search Edition! First edition with our new colleagues from Expand Online👌💪🏻 #tamtamhome #tttalks #breakfast #search</p>
                    </div>

                </div>
            </section>
        )
    }
}
