import React from "react";

import Slides from "../slider/Slides"
import BtnLeft from "../slider/BtnLeft";
import BtnView from "../slider/BtnView";
import BtnRight from "../slider/BtnRight";

export default class Slider extends React.Component {
    constructor() {
        super();
        this.handleLeftClick = this.handleLeftClick.bind(this);
        this.handleRightClick = this.handleRightClick.bind(this);
        this.state = {
            slideIndex: 1
        }
    }

    handleLeftClick() {
        this.state.slideIndex == 1 ? this.setState({slideIndex: this.props.slides.length}) :
        this.setState((prevState) => {
            return {slideIndex: prevState.slideIndex - 1};
        });
    }

    handleRightClick() {
        this.state.slideIndex == this.props.slides.length ? this.setState({slideIndex: 1}) :
        this.setState((prevState) => {
            return {slideIndex: prevState.slideIndex + 1};
        });
    }

    render() {
        const slideIndex = this.state.slideIndex;
        return (
            <section class="slider">
                <Slides slides={this.props.slides[slideIndex-1]}/>
                <h1 class="client">{this.props.titles[slideIndex-1]}</h1>
                <div class="buttons">
                    <BtnLeft onClick={this.handleLeftClick}/>
                    <BtnView/>
                    <BtnRight onClick={this.handleRightClick}/>
                </div>
                <a href="#" class="btn-scroll">
                    <div class="scroll-icon"></div>
                </a>
            </section>
        )
    }
}