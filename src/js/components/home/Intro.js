import React from "react";

export default class Intro extends React.Component {
    render() {
        return (
            <section class="we-are">
                <h1 class="title">We are Tam Tam</h1>
                <p class="paragraph">
                    Tam Tam is a full service digital agency focusing on Dutch Digital Service Design. We combine strategy, design, technology and interaction to make the digital interactions between company and customer valuable and memorable. We work for awesome brands with a team of 120 digitals from our office in Amsterdam. Making great work and having a blast doing it. That’s what we believe in.
                </p>
            </section>
        )
    }
}
