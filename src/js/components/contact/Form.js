require('./FormValidation');

import React from "react";

export default class Form extends React.Component {

    render() {
        return (
            <form class="form" noValidate>
                <div class="bg-layer"></div>
                <div class="field-wrapper">
                    <input type="text" id="fname" name="fname" placeholder="First name" pattern="[a-zA-Z]{3,}" required />
                    <span id="error-fname" class="error" aria-live="polite"></span>
                </div>
                <div class="field-wrapper">
                    <input type="text" id="lname" name="lname" placeholder="Last name" pattern="[a-zA-Z]{3,}" required />
                    <span id="error-lname" class="error" aria-live="polite"></span>
                </div>
                <div class="field-wrapper">
                    <input type= "email" id="mail" name="mail" placeholder="Your e-mail address" required />
                    <span id="error-mail" class="error" aria-live="polite"></span>
                </div>
                <div class="field-wrapper">
                    <input type= "tel" id="phone" name="phone" placeholder="Your phone number (optional)"/>
                </div>
                <div class="field-wrapper">
                    <textarea id="message" name="message" placeholder="Your message…" required></textarea>
                    <span id="error-message" class="error" aria-live="polite"></span>
                </div>
                <input class="send" type="submit" value="Send"/>
            </form>
        )
    }
}