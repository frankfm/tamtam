import React from "react";

export default class ErrorMessage extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <p class="error-message">
                    <span>
                        <img src="assets/png/error.png" alt="error-message"/>
                    </span>
                Please complete the form and try again
            </p>
        )
    }
}

