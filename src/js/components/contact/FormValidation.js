window.onload = () => {
    var checkIcon = '<img class="check-icon" src="assets/png/check-icon.png" alt="check-icon"/>';
    var errorMessage = document.getElementsByClassName('error-message')[0];
    var form  = document.getElementsByTagName('form')[0];
    var reqs = document.querySelectorAll('[required]');


    reqs.forEach(function(item) {
        item.addEventListener("keyup", function (event) {
            let parent = item.parentNode;
            let nextSibling = item.nextSibling;
            while(nextSibling && nextSibling.nodeType != 1) {
                nextSibling = nextSibling.nextSibling
            }
            if (item.validity.valid) {
                nextSibling.innerHTML = ""; // Reset the content of the message
                nextSibling.className = "error"; // Reset the visual state of the message
                parent.insertAdjacentHTML('beforeend', checkIcon);
            }
        }, false);
    });

    form.addEventListener("submit", function (event) {
        for (let i = 0 ; i < reqs.length; i++) {
            let item = reqs[i];
            let nextSibling = reqs[i].nextSibling;
            while(nextSibling && nextSibling.nodeType != 1) {
                nextSibling = nextSibling.nextSibling
            }
            if(!item.validity.valid) {
                errorMessage.className = "error-message active";
                nextSibling.className = "error active"; // Reset the visual state of the message
                if ( i == 0) {
                    nextSibling.innerHTML = "We need your first name.";
                } else if ( i == 1) {
                    nextSibling.innerHTML = "We need your last name.";
                } else if ( i == 2) {
                    nextSibling.innerHTML = "Please use a valid e-mail address.";
                } else {
                    nextSibling.innerHTML = "Sorry, your message can’t be empty.";
                }
                event.preventDefault();
            }
        }
    }, false);
}