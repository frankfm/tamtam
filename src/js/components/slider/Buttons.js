import React from "react";

import BtnLeft from "./BtnLeft";
import BtnView from "./BtnView";
import BtnRight from "./BtnRight";

export default class Buttons extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <div class="buttons">
                <BtnLeft/>
                <BtnView/>
                <BtnRight/>
            </div>
        )
    }
}
