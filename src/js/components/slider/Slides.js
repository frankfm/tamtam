import React from "react";

export default class Slides extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <div class="slides">{this.props.slides}</div>
        )
    }
}
