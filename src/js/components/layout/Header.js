import React from "react";

import Logo from "../header/Logo";
import Nav from "../header/Nav";
import NavToggle from "../header/NavToggle";

export default class Header extends React.Component {
    constructor() {
        super()
        this.state = {
            status : false,
            class: "navigation-main"
        }
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        if (this.state.status) {
            this.setState({
                status: false,
                class: "navigation-main"
            })
        } else {
            this.setState({
                status: true,
                class: "navigation-main open"
            });
        }
    }
    render() {
        return (
            <header class="header">
                <Logo />
                <Nav class={this.state.class} onClick={this.handleClick}/>
                <NavToggle onClick={this.handleClick}/>
            </header>
        );
    }
}