import React from "react";

export default class NavToggle extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <a class="navigation-toggle" onClick={() => this.props.onClick()}>
                <svg viewBox="0 0 24 22">
                    <g transform="translate(-163.000000, -678.000000)" id="UI_icons" fill="#FFFFFF">
                        <g transform="translate(150.000000, 527.000000)">
                            <g id="Icons" transform="translate(0.000000, 87.000000)">
                                <g id="menu" transform="translate(0.000000, 50.000000)">
                                    <path d="M13,14 L37,14 L37,17 L13,17 L13,14 Z M13,23.5 L37,23.5 L37,26.5 L13,26.5 L13,23.5 Z M13,32.5 L37,32.5 L37,35.5 L13,35.5 L13,32.5 Z"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </a>
        )
    }
}
