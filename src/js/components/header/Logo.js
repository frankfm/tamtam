import React from "react";

export default class Logo extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <a href="https://www.tamtam.nl/" class="logo">
                <svg viewBox="0 0 32 32">
                    <g transform="translate(-159.000000, -623.000000)"fill="#FFFFFF">
                        <g transform="translate(150.000000, 527.000000)">
                            <g transform="translate(0.000000, 87.000000)">
                                <g id="Logo">
                                    <path d="M15.3993966,41 L9,34.6012068 L34.6006034,9 L41,15.4018101 L15.3993966,41"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </a>
        )
    }
}
