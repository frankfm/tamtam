import React from "react";
import { Link, IndexLink } from "react-router";

export default class Nav extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <nav class={this.props.className}>
                <ul class="menu-main">
                    <li class="menu-item">
                        <IndexLink to="/" activeClassName="active" onClick={() => this.props.onClick()}>Home</IndexLink>
                    </li>
                    <li class="menu-item">
                        <Link to="people" activeClassName="active" onClick={() => this.props.onClick()}>People</Link>
                    </li>
                    <li class="menu-item">
                        <Link to="contact" activeClassName="active" onClick={() => this.props.onClick()}>Contact</Link>
                    </li>
                </ul>
            </nav>
        )
    }
}
